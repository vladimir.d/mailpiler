# Cloudron App

It contains a generic config that can be used to build Mailpiler app

https://www.mailpiler.org

# Important

The postinstall script makes an encryption key for piler, eg. /etc/piler/piler.key.

IMPORTANT! Make sure you never lose/overwrite the key /etc/piler/piler.key otherwise you won't access your archive ever again. So whenever you upgrade be sure to keep your existing key file.


# Build & Install
Init image tag

```
Tag=cloudron-$(date +%s)
```


To build and push image to Gitlab registry:

```
$ docker build --platform linux/amd64 -f Dockerfile --no-cache . -t registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/mailpiler:$Tag && \
    docker push registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/mailpiler:$Tag
```


To install app on Cloudron:

```
$ cloudron install --no-wait --server YOUR-CLOUDRON-SERVER --location mailpiler --image registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/mailpiler:$Tag --token YOUR-GITLAB-TOKEN
```

Update app on Cloudron:

```
$ cloudron update --no-wait --no-backup --server YOUR-CLOUDRON-SERVER --app mosquitto  --image registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/mailpiler:$Tag --token YOUR-GITLAB-TOKEN
```

To remove image from registry:

```
$ docker rmi -f registry.gitlab.com/YOUR-GITLAB-PROJECT-URL/mailpiler:$Tag
```
