FROM ubuntu:focal

ENV DEBIAN_FRONTEND="noninteractive" \
    DISTRO="focal" \
    DOWNLOAD_URL="https://download.mailpiler.com" \
    PILER_USER="piler" \
	PILER_GROUP="piler" \
    SPHINX_BIN_TARGZ="sphinx-3.3.1-bin.tar.gz" \
	PACKAGE="piler_1.3.11-focal-5c2ceb1_amd64.deb" \
	MEMCACHE_HOST="127.0.0.1" \
	MEMCACHE_PORT="11211" \
	PHP_INI="/etc/php/7.4/fpm/php.ini"

RUN apt-get update && \
    apt-get -y --no-install-recommends install \
       wget openssl sysstat php7.4-cli php7.4-cgi php7.4-mysql php7.4-fpm php7.4-zip php7.4-ldap \
       php7.4-gd php7.4-curl php7.4-xml php7.4-memcached catdoc unrtf poppler-utils nginx tnef sudo libzip5 \
       libtre5 cron libmariadb-dev mariadb-client-core-10.3 python3 python3-mysqldb ca-certificates curl \
	   crudini vim net-tools supervisor memcached && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    wget --no-check-certificate -q -O ${SPHINX_BIN_TARGZ} ${DOWNLOAD_URL}/generic-local/${SPHINX_BIN_TARGZ} && \
    tar zxvf ${SPHINX_BIN_TARGZ} && \
    sed -i '/session    required     pam_loginuid.so/c\#session    required     pam_loginuid.so' /etc/pam.d/cron && \
	wget --no-check-certificate -q -O ${PACKAGE} https://bitbucket.org/jsuto/piler/downloads/${PACKAGE} && \
    dpkg -i ${PACKAGE} && \
    ln -sf /etc/piler/piler-nginx.conf /etc/nginx/sites-enabled/ && \
    rm -f ${PACKAGE} ${SPHINX_BIN_TARGZ} /etc/nginx/sites-enabled/default /etc/piler/piler.key /etc/piler/piler.pem /etc/piler/config-site.php

# configure php
RUN crudini --set ${PHP_INI} PHP upload_max_filesize 256M && \
    crudini --set ${PHP_INI} PHP post_max_size 256M && \
    crudini --set ${PHP_INI} PHP memory_limit 512M && \
    crudini --set ${PHP_INI} Session session.save_path /run/app/sessions && \
    crudini --set ${PHP_INI} Session session.gc_probability 1 && \
    crudini --set ${PHP_INI} Session session.gc_divisor 100

# configure cron & clear out the crontab
RUN ln -s /run/cron /var/spool/cron && \
	rm -f /etc/cron.d/* /etc/cron.daily/* /etc/cron.hourly/* /etc/cron.monthly/* /etc/cron.weekly/* && truncate -s0 /etc/crontab

RUN mkdir -p /app/data/piler/config /app/data/piler/data && \
	cp -R /etc/piler/* /app/data/piler/config && \
	rm -rf /etc/piler && \
	ln -sf /app/data/piler/config /etc/piler && \

	cp /usr/share/piler/piler.cron /app/data/piler/ && \
	rm -rf /usr/share/piler/piler.cron && \
	ln -sf /app/data/piler/piler.cron /usr/share/piler/piler.cron && \
    crontab -u $PILER_USER /usr/share/piler/piler.cron && \

	cp -R /var/piler/* /app/data/piler/data && \
	rm -rf /var/piler && \
	ln -sf /app/data/piler/data /var/piler && \
	chown -R $PILER_USER:$PILER_GROUP /app/data/piler 

# nginx
RUN mkdir -p /app/data/nginx/conf/sites-enabled && \
	rm -rf /etc/nginx/sites-enabled && \
    ln -s /app/data/nginx/conf/sites-enabled /etc/nginx/sites-enabled && \
	ln -sf /etc/piler/piler-nginx.conf /etc/nginx/sites-enabled/ && \
    rm -rf /var/lib/nginx/ && \
    ln -sf /run/nginx /var/lib/nginx && \
    ln -sf /run/nginx-error.log /var/log/nginx/error.log && \
    ln -sf /run/nginx-access.log /var/log/nginx/access.log

EXPOSE 25 80 

# supervisor
ADD config/supervisor/ /app/data/supervisor/
RUN rm -rf /etc/supervisor/supervisord.conf && \
    rm -rf /etc/supervisor/conf.d/ && \
    ln -sf /app/data/supervisor/supervisord.conf /etc/supervisor/supervisord.conf && \
    ln -sf /app/data/supervisor/conf.d /etc/supervisor/conf.d && \
    ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log && \
	ln -sf /run/php7.4-fpm.log /var/log/php7.4-fpm.log

COPY start.sh /app/pkg/start.sh

CMD ["/app/pkg/start.sh"]
