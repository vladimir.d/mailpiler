#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

CONFIG_DIR="/etc/piler"
VOLUME_DIR="/var/piler"
PILER_CONF="${CONFIG_DIR}/piler.conf"
CLOUDRON_TLS_CERT_PEM="/etc/certs/tls_cert.pem"
CLOUDRON_TLS_CERT_KEY="/etc/certs/tls_key.pem"
PILER_KEY="${CONFIG_DIR}/piler.key"
PILER_PEM="${CONFIG_DIR}/piler.pem"
PILER_NGINX_CONF="${CONFIG_DIR}/piler-nginx.conf"
SPHINX_CONF="${CONFIG_DIR}/sphinx.conf"
CONFIG_SITE_PHP="${CONFIG_DIR}/config-site.php"
PILER_MY_CNF="${CONFIG_DIR}/.my.cnf"


error() {
   echo "ERROR:" "$*" 1>&2
   exit 1
}


log() {
   echo "DEBUG:" "$*"
}


pre_flight_check() {
   [[ -v CLOUDRON_APP_DOMAIN ]] || error "Missing CLOUDRON_APP_DOMAIN env variable"
   [[ -v CLOUDRON_MYSQL_HOST ]] || error "Missing CLOUDRON_MYSQL_HOST env variable"
   [[ -v CLOUDRON_MYSQL_DATABASE ]] || error "Missing CLOUDRON_MYSQL_DATABASE env variable"
   [[ -v CLOUDRON_MYSQL_USERNAME ]] || error "Missing CLOUDRON_MYSQL_USERNAME env variable"
   [[ -v CLOUDRON_MYSQL_PASSWORD ]] || error "Missing CLOUDRON_MYSQL_PASSWORD env variable"
}


give_it_to_piler() {
   local f="$1"

   [[ -f "$f" ]] || error "${f} does not exist, aborting"

   chown "${PILER_USER}:${PILER_USER}" "$f"
   chmod 600 "$f"
}

generate_certificate() {
   local f="$1"

   local crt="/tmp/1.cert"
   local SSL_CERT_DATA="/C=US/ST=Denial/L=Springfield/O=Dis/CN=${CLOUDRON_APP_DOMAIN}"

   log "Making an ssl certificate"

   openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj "$SSL_CERT_DATA" -keyout "$f" -out "$crt" -sha1 2>/dev/null
   cat "$crt" >> "$f"
   rm -f "$crt"

   give_it_to_piler "$f"
}


make_certificate() {
   local f="$1"

   if [[ -f "$CLOUDRON_TLS_CERT_PEM" ]]; then
      log "Use $CLOUDRON_TLS_CERT_PEM certificate"
      ln -sf "$CLOUDRON_TLS_CERT_PEM" "$f"
   else
      generate_certificate "$f"
   fi
}


make_piler_key() {
   local f="$1"

   log "Generating piler.key"

   dd if=/dev/urandom bs=56 count=1 of="$f" 2>/dev/null
   [[ $(stat -c '%s' "$f") -eq 56 ]] || error "could not read 56 bytes from /dev/urandom to ${f}"

   give_it_to_piler "$f"
}

fix_configs() {
   [[ -f "$PILER_KEY" ]] || make_piler_key "$PILER_KEY"
   [[ -f "$PILER_PEM" ]] || make_certificate "$PILER_PEM"

   if [[ ! -f "$PILER_NGINX_CONF" ]]; then
      log "Writing ${PILER_NGINX_CONF}"

      cp "${PILER_NGINX_CONF}.dist" "$PILER_NGINX_CONF"
      sed -i "s%PILER_HOST%${CLOUDRON_APP_DOMAIN}%" "$PILER_NGINX_CONF"
   fi

   if [[ ! -f "$PILER_CONF" ]]; then
      log "Writing ${PILER_CONF}"

      sed \
         -e "s/mysqluser=.*/mysqluser=${CLOUDRON_MYSQL_USERNAME}/g" \
         -e "s/mysqldb=.*/mysqldb=${CLOUDRON_MYSQL_DATABASE}/g" \
         -e "s/verystrongpassword/${CLOUDRON_MYSQL_PASSWORD}/g" \
         -e "s/hostid=.*/hostid=${CLOUDRON_APP_DOMAIN}/g" \
         -e "s/tls_enable=.*/tls_enable=1/g" \
         -e "s/mysqlsocket=.*/mysqlsocket=/g" "${PILER_CONF}.dist" > "$PILER_CONF"

      {
         echo "mysqlhost=${CLOUDRON_MYSQL_HOST}"
      } >> "$PILER_CONF"

      give_it_to_piler "$PILER_CONF"
   fi

   if [[ ! -f "$CONFIG_SITE_PHP" ]]; then
      log "Writing ${CONFIG_SITE_PHP}"

      cp "${CONFIG_DIR}/config-site.dist.php" "$CONFIG_SITE_PHP"

      sed -i "s%HOSTNAME%${CLOUDRON_APP_DOMAIN}%" "$CONFIG_SITE_PHP"

      {
         echo "\$config['DECRYPT_BINARY'] = '/usr/bin/pilerget';"
         echo "\$config['DECRYPT_ATTACHMENT_BINARY'] = '/usr/bin/pileraget';"
         echo "\$config['PILER_BINARY'] = '/usr/sbin/piler';"
         echo "\$config['DB_HOSTNAME'] = '$CLOUDRON_MYSQL_HOST';"
         echo "\$config['DB_DATABASE'] = '$CLOUDRON_MYSQL_DATABASE';"
         echo "\$config['DB_USERNAME'] = '$CLOUDRON_MYSQL_USERNAME';"
         echo "\$config['DB_PASSWORD'] = '$CLOUDRON_MYSQL_PASSWORD';"
         echo "\$config['ENABLE_MEMCACHED'] = 1;"
         echo "\$memcached_server = ['$MEMCACHE_HOST', $MEMCACHE_PORT];"

         echo "\$config['ENABLE_LDAP_AUTH'] = 1;"
         echo "\$config['LDAP_HOST'] = 'ldap://$CLOUDRON_LDAP_SERVER:$CLOUDRON_LDAP_PORT';"
         echo "\$config['LDAP_BASE_DN'] = '${CLOUDRON_LDAP_USERS_BASE_DN}';"
         echo "\$config['LDAP_HELPER_DN'] = '$CLOUDRON_LDAP_BIND_DN';"
         echo "\$config['LDAP_HELPER_PASSWORD'] = '$CLOUDRON_LDAP_BIND_PASSWORD';"
         echo "\$config['LDAP_MAIL_ATTR'] = 'mail';"
         echo "\$config['LDAP_ADMIN_MEMBER_DN'] = 'cn=admins,$CLOUDRON_LDAP_GROUPS_BASE_DN';"
         # auth by username
         #echo "\$config['LDAP_ACCOUNT_OBJECTCLASS'] = 'username';"
         # auth by email
         echo "\$config['LDAP_ACCOUNT_OBJECTCLASS'] = 'user';"
      } >> "$CONFIG_SITE_PHP"
   fi

   sed -e "s%MYSQL_HOSTNAME%${CLOUDRON_MYSQL_HOST}%" \
       -e "s%MYSQL_DATABASE%${CLOUDRON_MYSQL_DATABASE}%" \
       -e "s%MYSQL_USERNAME%${CLOUDRON_MYSQL_USERNAME}%" \
       -e "s%MYSQL_PASSWORD%${CLOUDRON_MYSQL_PASSWORD}%" \
       -i "$SPHINX_CONF"
}

wait_until_mysql_server_is_ready() {
   while true; do if mysql "--defaults-file=${PILER_MY_CNF}" <<< "show databases"; then break; fi; log "${CLOUDRON_MYSQL_HOST} is not ready"; sleep 5; done

   log "${CLOUDRON_MYSQL_HOST} is ready"
}


init_database() {
   local table
   local has_metadata_table=0

   wait_until_mysql_server_is_ready

   while read -r table; do
      if [[ "$table" == metadata ]]; then has_metadata_table=1; fi
   done < <(mysql "--defaults-file=${PILER_MY_CNF}" "$CLOUDRON_MYSQL_DATABASE" <<< 'show tables')

   if [[ $has_metadata_table -eq 0 ]]; then
      log "no metadata table, creating tables"

      mysql "--defaults-file=${PILER_MY_CNF}" "$CLOUDRON_MYSQL_DATABASE" < /usr/share/piler/db-mysql.sql
   else
      log "metadata table exists"
   fi

   if [[ -v ADMIN_USER_PASSWORD_HASH ]]; then
      mysql "--defaults-file=${PILER_MY_CNF}" "$CLOUDRON_MYSQL_DATABASE" <<< "update user set password='${ADMIN_USER_PASSWORD_HASH}' where uid=0"
   fi
}


create_my_cnf_files() {
   printf "[client]\nhost = %s\nuser = %s\npassword = %s\n[mysqldump]\nhost = %s\nuser = %s\npassword = %s\n" \
      "$CLOUDRON_MYSQL_HOST" "$CLOUDRON_MYSQL_USERNAME" "$CLOUDRON_MYSQL_PASSWORD" "$CLOUDRON_MYSQL_HOST" "$CLOUDRON_MYSQL_USERNAME" "$CLOUDRON_MYSQL_PASSWORD" \
      > "$PILER_MY_CNF"

   give_it_to_piler "$PILER_MY_CNF"
}


start_services() {
   
   mkdir -p /run/nginx /run/cron /run/app/sessions

   chown -R www-data:www-data /run/app

   service cron start
   service memcached start
   service php7.4-fpm start
   service nginx start
}


start_piler() {
   if [[ ! -f "${VOLUME_DIR}/sphinx/main1.spp" ]]; then
      log "main1.spp does not exist, creating index files"
      su -c "indexer --all --config ${SPHINX_CONF}" piler
   fi

   # No pid file should exist for piler
   rm -f /var/run/piler/*pid

   /etc/init.d/rc.searchd start
   /etc/init.d/rc.piler start
}


pre_flight_check
fix_configs
create_my_cnf_files
init_database
start_services
start_piler

#echo "Starting Mailpiler"
#exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i MailPiler

while true; do sleep 3600; done
